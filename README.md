# Regulus

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://gcollet.gitlab.io/Regulus.jl/dev)
[![Build Status](https://gitlab.com/gcollet/Regulus.jl/badges/master/pipeline.svg)](https://gitlab.com/gcollet/Regulus.jl/pipelines)
[![Coverage](https://gitlab.com/gcollet/Regulus.jl/badges/master/coverage.svg)](https://gitlab.com/gcollet/Regulus.jl/commits/master)
