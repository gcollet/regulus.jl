using Regulus
using Documenter

DocMeta.setdocmeta!(Regulus, :DocTestSetup, :(using Regulus); recursive=true)

makedocs(;
    modules=[Regulus],
    authors="Guillaume Collet <guillaume.collet@univ-rennes1.fr> and contributors",
    repo="https://gitlab.com/gcollet/Regulus.jl/blob/{commit}{path}#{line}",
    sitename="Regulus.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://gcollet.gitlab.io/Regulus.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
