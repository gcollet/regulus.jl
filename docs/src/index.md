```@meta
CurrentModule = Regulus
```

# Regulus

Documentation for [Regulus](https://gitlab.com/gcollet/Regulus.jl).

```@index
```

```@autodocs
Modules = [Regulus]
```
